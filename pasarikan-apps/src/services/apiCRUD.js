import axios from "axios";
// const axios = require('axios').default;

export default {
  getList() {
    return axios
      .get("https://pasarikan.herokuapp.com/crud/list")
      .then(value => value);
  },
  create(req) {
    return axios
      .post("https://pasarikan.herokuapp.com/crud/create", req)
      .then(value => value);
  },
  detail(req) {
    return axios
      .post("https://pasarikan.herokuapp.com/crud/detail", req)
      .then(value => value);
  },
  update(req) {
    return axios
      .post("https://pasarikan.herokuapp.com/crud/update", req)
      .then(value => value);
  },
  delete(req) {
    return axios
      .post("https://pasarikan.herokuapp.com/crud/delete", req)
      .then(value => value);
  }
};
