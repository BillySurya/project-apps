import Vue from "vue";
import Vuex from "vuex";

import storeCRUD from "./modules/storeCRUD";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: { storeCRUD },
  strict: true
});
