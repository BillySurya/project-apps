import api from "@/services/apiCRUD";

const state = {
  list_user: [],
  success_create: null,
  success_edit: null,
  success_detail: null,
  success_delete: null
};

const getters = {};

const mutations = {
  // eslint-disable-next-line no-shadow
  setList(state, data) {
    state.list_user = data.data.data;
  },
  successCreateList(state, data) {
    state.success_create = data.data.data;
  },
  successUpdateList(state, data) {
    state.success_edit = data.data.data;
  },
  successGetDetail(state, data) {
    state.success_detail = data.data.data;
  },
  successDeleteList(state, data) {
    state.success_delete = data.data.data;
  }
};

const actions = {
  async getList({ commit }) {
    try {
      const res = await api.getList();
      commit("setList", res);
    } catch (err) {
      //Action Error Here
    }
  },
  async create({ commit }, req) {
    try {
      const res = await api.create(req);
      commit("successCreateList", res);
    } catch (err) {
      //Action Error Here
    }
  },
  async update({ commit }, req) {
    try {
      const res = await api.update(req);
      commit("successUpdateList", res);
    } catch (err) {
      //Action Error Here
    }
  },
  async detail({ commit }, req) {
    try {
      const res = await api.detail(req);
      commit("successGetDetail", res);
    } catch (err) {
      //Action Error Here
    }
  },
  async delete({ commit }, req) {
    try {
      const res = await api.delete(req);
      commit("successDeleteList", res);
    } catch (err) {
      //Action Error Here
    }
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
