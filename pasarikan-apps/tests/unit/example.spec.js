import { mount , shallowMount } from "@vue/test-utils";
import UserProfile from "@/pages/UserProfile.vue";

describe('UserProfile', () => {
  // Now mount the component and you have the wrapper
  const wrapper = mount(UserProfile)

  it('renders the correct markup', () => {
    expect(wrapper.html()).toContain('<label>Name</label>')
  })

  // it's also easy to check for the existence of elements
  it('has a button', () => {
    expect(wrapper.contains('md-input')).toBe(true)
  })
})